package app

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/josafat.vargas.gamboa/utils_go/loggers"
)

var router = mux.NewRouter()

func StartAppliaction() {
	loggers.Info("+++ Starting application")
	mapURLs()

	srv := &http.Server{
		Handler: router,
		Addr:    "127.0.0.1:9002",
		// WriteTimeout: 15*time.Second,
		// ReadTimeout: 15*time.Second,
	}

	if err := srv.ListenAndServe(); err != nil {
		panic(err)
	}
}
