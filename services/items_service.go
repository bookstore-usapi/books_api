package services

import (
	"gitlab.com/josafat.vargas.gamboa/books_api/domain/items"
	"gitlab.com/josafat.vargas.gamboa/utils_go/rest_errors"
)

// Public instance: a pointer to the "Class" (struct). {} means no attributes
var ItemsService itemsServiceInterface = &itemsService{}

// Abstract methods of the "Class"
type itemsServiceInterface interface {
	CreateItem(items.Item) (*items.Item, *rest_errors.RestErr)
	GetItem(string) (*items.Item, *rest_errors.RestErr)
}

// Items service "Class" with no attributes
type itemsService struct {
}

// Implemention of the methods

// Pointer referst the func as a method of itemsService
func (s *itemsService) CreateItem(item items.Item) (*items.Item, *rest_errors.RestErr) {
	return nil, rest_errors.NewBadRequestError("Not Implemented")
}

func (s *itemsService) GetItem(itemId string) (*items.Item, *rest_errors.RestErr) {
	return nil, rest_errors.NewBadRequestError("Not Implemented")
}
