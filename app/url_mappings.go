package app

import (
	"net/http"

	"gitlab.com/josafat.vargas.gamboa/books_api/controllers"
)

func mapURLs() {
	router.HandleFunc("/ping", controllers.PingController.Ping).Methods(http.MethodGet)

	router.HandleFunc("/items", controllers.ItemsController.Create).Methods(http.MethodPost)
	router.HandleFunc("/items/:item_id", controllers.ItemsController.Get).Methods(http.MethodGet)

}
