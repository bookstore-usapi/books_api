package controllers

import (
	"fmt"
	"net/http"

	"gitlab.com/josafat.vargas.gamboa/books_api/domain/items"
	"gitlab.com/josafat.vargas.gamboa/books_api/services"
	"gitlab.com/josafat.vargas.gamboa/oauth_go/oauth_go/oauth"
)

// AKA as handlers in Gorilla/Mux

var ItemsController itemsControllerInterface = &itemsController{}

// Interface allows us to segregate the Methods into a type
type itemsControllerInterface interface {
	Create(http.ResponseWriter, *http.Request)
	Get(http.ResponseWriter, *http.Request)
}

type itemsController struct{}

// Pointers means we call the methods from an instance
func (c *itemsController) Create(w http.ResponseWriter, r *http.Request) {

	if err := oauth.AuthenticateRequest(r); err != nil {
		// TODO: Return error
		return
	}

	item := items.Item{
		Seller: oauth.GetClientId(r),
	}

	result, err := services.CreateItem(item)
	if err != nil {
		return
	}

	fmt.Println("Created", result)
	// TODO: return JSON 201 created

}

func (c *itemsController) Get(w http.ResponseWriter, r *http.Request) {

}
