# Bookstore API main

All you need to integrate SQL & NoSQL databases, search engines and all the tools that you need in your REST API using microservices.
Developed with Go bsed on: https://kyndryl.udemy.com/course/golang-how-to-design-and-build-rest-microservices-in-go/

### Users API

Gin
MVC pattern
MySQL

src: https://github.com/federicoleon/bookstore_users-api

### oAuth API

Gon
Domain Driven Design
CassandraDB

src: https://github.com/federicoleon/bookstore_oauth-api

### Items API

Gorilla/mux
MVC
ElasticSearch

src: https://github.com/federicoleon/bookstore_items-api

### Utils shared library

src: https://github.com/federicoleon/bookstore_utils-go


### OAuth common library
